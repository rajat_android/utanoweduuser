package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ProviderDetailsResponse;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.UI.Fragments.BottomSheetFragment;

public class SinglDoctorScreen extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ProviderDetailsResponse.Object object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singl_doctor_screen);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        object = (ProviderDetailsResponse.Object) getIntent().getSerializableExtra("object");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(Double.parseDouble(object.getLattitude()), Double.parseDouble(object.getLongitude()));
        mMap.addMarker(new MarkerOptions().position(sydney).title("Provider Location"));
        moveToLocation(sydney);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("doctorData", object);
                bottomSheetFragment.setArguments(bundle);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                return true;
            }
        });
    }



    private void moveToLocation(LatLng location) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }
}
