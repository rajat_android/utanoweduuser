package user.udanutwedu.doctor.utanowedu_user.Utilties;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.Date;

public class Message implements IMessage {

    String text, id, time;
    Date date;
    Author author;
    public Message(String text, String id, Date date, Author author) {
        this.text = text;
        this.id = id;
        this.date = date;
        this.author = author;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public IUser getUser() {
        return author;
    }

    @Override
    public Date getCreatedAt() {
        return date;
    }
}
