package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.util.Objects;

import io.fabric.sdk.android.Fabric;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginResponse;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.Utilties.GlobalData;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.PREF_NAME;


public class Splash extends AppCompatActivity {

    LoginResponse.Data data;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Fabric.with(this, new Crashlytics());
        Objects.requireNonNull(getSupportActionBar()).hide();
        Gson gson = new Gson();
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String stringData = sharedPreferences.getString("data", "");
        data = gson.fromJson(stringData, LoginResponse.Data.class);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(data==null){
                    Intent i = new Intent(Splash.this, LoginActivity.class);
                    startActivity(i);
                }
                else{
                    GlobalData.data = data;
                    startActivity(new Intent(Splash.this, HomeActivity.class));
                }
                finish();
            }
        }, 2000);
    }
}
