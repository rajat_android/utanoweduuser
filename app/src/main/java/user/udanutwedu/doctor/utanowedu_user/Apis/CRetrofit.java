package user.udanutwedu.doctor.utanowedu_user.Apis;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import user.udanutwedu.doctor.utanowedu_user.Interfaces.Api;

public class CRetrofit {

    static String BASE_URL = "http://100.27.29.67:3001/";

    public static Api api = null;

    private CRetrofit(){

    }

    public static Api getInstance(){
        if(api==null){
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();
            Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
                    .client(client).build();
            api = retrofit.create(Api.class);
        }
        return api;
    }
}
