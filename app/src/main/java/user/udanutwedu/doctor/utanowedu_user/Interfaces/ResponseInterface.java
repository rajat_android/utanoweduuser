package user.udanutwedu.doctor.utanowedu_user.Interfaces;

public interface ResponseInterface {

    void success();

    void newtworkIssue(String message);

    void otherMessage(String message);
}
