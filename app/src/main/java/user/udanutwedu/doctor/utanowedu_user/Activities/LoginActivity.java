package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.udanutwedu.doctor.utanowedu_user.Apis.CRetrofit;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginBody;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginResponse;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.Utilties.GlobalData;
import user.udanutwedu.doctor.utanowedu_user.Utilties.MyFunctions;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_STATE;
import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.PREF_NAME;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText cellNumber, password;
    private ProgressDialog progressDialog;
    private Call<LoginResponse> call;
    private SharedPreferences sharedPreferences;
    private LocationManager manager;
    private TextView forgotPass;
    private String[] countryCodes = {"+263", "+91"};
    private Spinner codeSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        getSupportActionBar().setTitle(R.string.login);
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        findViewById(R.id.loginButton).setOnClickListener(this);
        findViewById(R.id.registrationPath).setOnClickListener(this);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(getApplicationContext());
        }
        cellNumber = findViewById(R.id.cell_number);
        if (checkPermission(this, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION, CALL_PHONE)) {
        } else {
            requestPermission(this, ACCESS_FINE_LOCATION, CALL_PHONE);
        }
        codeSpinner = findViewById(R.id.codeSpinner);
        codeSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countryCodes));
        password = findViewById(R.id.password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Logging In!");
        forgotPass = findViewById(R.id.forgotPass);
        forgotPass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginButton:
                if(TextUtils.isEmpty(cellNumber.getText().toString())){
                    cellNumber.setError("Cell Number is empty!");
                }
                else if(TextUtils.isEmpty(password.getText().toString())){
                    password.setError("Password is empty!");
                }
                else{
                    if(checkPermission(this, ACCESS_FINE_LOCATION, CALL_PHONE)){
                        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                            displayLocationSettingsRequest(this);
                        }
                        else{
                            progressDialog.show();
                            login();
                        }
                    }
                    else{
                        requestPermission(this, ACCESS_FINE_LOCATION, CALL_PHONE);
                    }
                }
                break;
            case R.id.registrationPath:
                if(checkPermission(this, ACCESS_FINE_LOCATION, CALL_PHONE)){
                    if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                        displayLocationSettingsRequest(this);
                    }
                    else{
                        startActivity(new Intent(this, RegisterationActivity.class));
                    }
                }
                else{
                    requestPermission(this, ACCESS_FINE_LOCATION, CALL_PHONE);
                }
                break;
            case R.id.forgotPass:
                startActivity(new Intent(this, ForgotActivity.class));
                break;
        }
    }

    private void login() {
        call = CRetrofit.getInstance().login(new LoginBody(codeSpinner.getSelectedItem().toString()+cellNumber.getText().toString().trim(),
                password.getText().toString(), "user"));
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.code()==200){
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    Gson gson = new Gson();
                    String data = gson.toJson(response.body().getData());
                    editor.putString("data", data);
                    GlobalData.data = response.body().getData();
                    editor.apply();
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                }
                else{
                    try {
                        MyFunctions.dialogdontfinish(new JSONObject(response.errorBody().string()).getString("message"), LoginActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("throw", t.toString());
                Toast.makeText(LoginActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(call!=null){
            call.cancel();
        }
    }

    public static boolean checkPermission(Context context, String... permissions) {
        boolean chk = false;
        for (String permission : permissions) {
            chk = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        }
        return chk;
    }

    public static void requestPermission(Context context, String... permissions) {
        ActivityCompat.requestPermissions((AppCompatActivity) context, permissions,
                AppCompatActivity.RESULT_FIRST_USER);
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000/2);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(LoginActivity.this,
                                6);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.terms:
                startActivity(new Intent(this, TermsActivity.class)
                .putExtra("name", getString(R.string.policy)));
                break;
            case R.id.privacy_policy:
                startActivity(new Intent(this, TermsActivity.class)
                        .putExtra("name", getString(R.string.terms)));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.loginmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
