package user.udanutwedu.doctor.utanowedu_user.UI.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Arrays;

import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.UI.Adapter.DoctorNameAdapter;

public class DoctorFragment extends Fragment {

    RecyclerView recyclerView;
    ListView doctorList;
    String doctorListItems[] = {"Anesthetists", "Dentists", "Dermatologists",
            "Ear Nose and Throat",
            "Family Practitioner",
            "Haematologists",
            "Forensic Pathologists",
            "General Surgeons",
            "Neurosurgeons",
            "Obstetrician and Gynecologists",
             "Ophthalmologists",
             "Oral and Maxillofacial Pathologists",
             "Oral and Maxillofacial Surgeons",
             "Orthodontists",
             "Orthopedic Surgeons",
             "Paediatricians",
             "Pathologists",
             "Periodontists",
             "Prosthodontists",
             "Physicians",
             "Plastic Surgeons",
             "Prosthodontists",
             "Psychiatrists",
             "Radiologists (Diagnostics)",
             "Radiotherapists and Oncologists",
             "Urologists",
             "Venereologists"};

    public DoctorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_doctor, container, false);
        /*doctorList = view.findViewById(R.id.doctor_list);
        doctorList.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, doctorNames));*/
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new DoctorNameAdapter(getActivity(), Arrays.asList(doctorListItems)));

        return view;
    }

}
