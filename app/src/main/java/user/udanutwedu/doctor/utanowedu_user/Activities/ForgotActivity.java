package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.udanutwedu.doctor.utanowedu_user.Apis.CRetrofit;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ChangePasswordResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ForgotModel;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.Utilties.MyFunctions;

public class ForgotActivity extends AppCompatActivity {

    private EditText cellNumber;
    private EditText email;
    private Button forgotBtn;
    private ProgressDialog progressDialog;
    private String[] countryCodes = {"+263", "+91"};
    private Spinner codeSpinner;
    private Call<ChangePasswordResponse> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        getSupportActionBar().setTitle(getString(R.string.forgotPassword));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cellNumber = findViewById( R.id.cell_number );
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        codeSpinner = findViewById(R.id.codeSpinner);
        codeSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countryCodes));
        email = findViewById( R.id.email );
        forgotBtn = findViewById( R.id.forgotBtn );
        forgotBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    forgotPassword();
                }
            }
        });
    }

    private void forgotPassword() {
        progressDialog.show();
        call = CRetrofit.getInstance().forgotPassword(new ForgotModel("user", codeSpinner.getSelectedItem().toString()+cellNumber.getText().toString().trim(),
                email.getText().toString().trim()));
        call.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                if(response.isSuccessful()){
                    new AlertDialog.Builder(ForgotActivity.this)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).setCancelable(false)
                            .setMessage(response.body().getMessage())
                            .show();
                }
                else{
                    try {
                        MyFunctions.dialogdontfinish(new JSONObject(response.errorBody().string()).getString("message"), ForgotActivity.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                Log.e("throw", t.toString());
                Toast.makeText(ForgotActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private boolean validate() {
        boolean isDataOk = true;
        if(TextUtils.isEmpty(cellNumber.getText().toString().trim()) || cellNumber.getText().toString().trim().length()!=9){
            cellNumber.setError(getString(R.string.invalid_number));
            isDataOk = false;
        }
        if (TextUtils.isEmpty(email.getText().toString().trim()) || !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
            email.setError(getString(R.string.invalid_email));
            isDataOk = false;
        }
        return isDataOk;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(call!=null){
            call.cancel();
        }
    }
}
