package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.utilities.Utilities;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.udanutwedu.doctor.utanowedu_user.Apis.CRetrofit;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ChangePasswordModel;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ChangePasswordResponse;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.Utilties.GlobalData;
import user.udanutwedu.doctor.utanowedu_user.Utilties.MyFunctions;

public class ChangePassword extends AppCompatActivity {

    private EditText oldPassword;
    private EditText newPassword;
    private EditText confirmPassword;
    private Button changeBtn;
    private ProgressDialog progressDialog;
    private Call<ChangePasswordResponse> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setTitle(R.string.change_pass);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        oldPassword = findViewById( R.id.oldPassword );
        newPassword = findViewById( R.id.newPassword );
        confirmPassword = findViewById( R.id.confirmPassword );
        changeBtn = findViewById( R.id.changeBtn );
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        changeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    changePassword();
                }
            }
        });
    }

    private void changePassword() {
        progressDialog.show();
        call = CRetrofit.getInstance().changePassword(new ChangePasswordModel("user",
                GlobalData.data.getCellNumber(), oldPassword.getText().toString().trim(), newPassword.getText().toString().trim()));
        call.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                if(response.isSuccessful()){
                    new AlertDialog.Builder(ChangePassword.this)
                            .setCancelable(false)
                            .setMessage(response.body().getMessage())
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }
                else{
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        MyFunctions.dialogdontfinish(jsonObject.getString("message"), ChangePassword.this);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                Toast.makeText(ChangePassword.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                Log.e("throw", t.toString());
                progressDialog.dismiss();
            }
        });
    }

    private boolean validate() {
        boolean isDataOk = true;
        if(TextUtils.isEmpty(oldPassword.getText().toString().trim())){
            oldPassword.setError(getString(R.string.mendatory_field));
            isDataOk = false;
        }
        if(TextUtils.isEmpty(newPassword.getText().toString().trim())){
            newPassword.setError(getString(R.string.mendatory_field));
            isDataOk = false;
        }
        if(TextUtils.isEmpty(confirmPassword.getText().toString().trim())){
            confirmPassword.setError(getString(R.string.mendatory_field));
            isDataOk = false;
        }
        if(!TextUtils.equals(newPassword.getText().toString().trim(), confirmPassword.getText().toString().trim())){
            Toast.makeText(this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
            isDataOk = false;
        }
        return isDataOk;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(call!=null){
            call.cancel();
        }
    }
}
