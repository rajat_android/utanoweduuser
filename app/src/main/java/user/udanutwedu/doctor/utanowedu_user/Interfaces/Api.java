package user.udanutwedu.doctor.utanowedu_user.Interfaces;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.AllergiesResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ChangePasswordModel;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ChangePasswordResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ForgotModel;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginBody;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ProviderDetailsResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.SignUpResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.SignUpModel;

public interface Api {

    String BASE_URL = "http://100.27.29.67:3001/";

    String CHAT_URL = "https://utanoweduchat-793fb.firebaseio.com/user";

    @GET("healthdata")
    Call<AllergiesResponse> getAllergiesList();

    @POST("auth/userSignup")
    Call<SignUpResponse> registerUser(@Body SignUpModel signUpModel);

    @POST("auth/login")
    Call<LoginResponse> login(@Body LoginBody loginBody);

    @GET("providers")
    Call<ProviderDetailsResponse> getProvidersList(@Query("providerType") String providerType,
                                                   @Query("specialization") String specialization);

    @PUT("auth/changePassword")
    Call<ChangePasswordResponse> changePassword(@Body ChangePasswordModel changePasswordModel);

    @PUT("auth/forgotPassword")
    Call<ChangePasswordResponse> forgotPassword(@Body ForgotModel forgotModel);
}
