package user.udanutwedu.doctor.utanowedu_user.UI.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import user.udanutwedu.doctor.utanowedu_user.R;

public class MoreFragment extends Fragment implements View.OnClickListener{

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        ((LinearLayout)view.findViewById( R.id.contactUs )).setOnClickListener(this);
        ((LinearLayout)view.findViewById( R.id.share )).setOnClickListener(this);
        ((LinearLayout)view.findViewById( R.id.terms )).setOnClickListener(this);
        ((LinearLayout)view.findViewById( R.id.privacy_policy )).setOnClickListener(this);
        ((LinearLayout)view.findViewById( R.id.rate_app )).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {

    }
}
