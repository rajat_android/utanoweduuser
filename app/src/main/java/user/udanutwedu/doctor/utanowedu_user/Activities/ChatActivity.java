package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.Utilties.Author;
import user.udanutwedu.doctor.utanowedu_user.Utilties.GlobalData;
import user.udanutwedu.doctor.utanowedu_user.Utilties.Message;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.MIX_TOKEN;

public class ChatActivity extends AppCompatActivity {

    MessagesList messagesList;
    MessageInput messageInput;
    MessagesListAdapter<Message> adapter;
    String sender = "sender", reciever = "reciever";
    ImageLoader imageLoader;
    ProgressBar progressBar;
    private MixpanelAPI mixpanelAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getSupportActionBar().setTitle(getIntent().getStringExtra("doctorName"));
        mixpanelAPI = MixpanelAPI.getInstance(this, MIX_TOKEN);
        mixpanelAPI.track("UserApp - In App Messaging");
        final Firebase reference = new Firebase("https://utanoweduchat-793fb.firebaseio.com/messages/"+ GlobalData.data.getCellNumber().replace("+", "")+getIntent().getStringExtra("doctorNumber").replace("+", ""));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String a = "yo";
        messagesList = findViewById(R.id.messagesList);
        messageInput = findViewById(R.id.input);
        progressBar = findViewById(R.id.progressBar);
        adapter = new MessagesListAdapter<>("a", null);
        messagesList.setAdapter(adapter);
        messageInput.setInputListener(new MessageInput.InputListener() {
            @Override
            public boolean onSubmit(CharSequence input) {
                String messageText = messageInput.getInputEditText().toString();
                if(!messageText.equals("")){
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("message", input.toString());
                    map.put("sender", GlobalData.data.getCellNumber().replace("+", ""));
                    map.put("time", ""+System.currentTimeMillis());
                    map.put("reciever", getIntent().getStringExtra("doctorNumber").replace("+", ""));
                    map.put("name", GlobalData.data.getFirstName()+" "+GlobalData.data.getSirName());
                    reference.push().setValue(map);
                }
                return true;
            }
        });

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                progressBar.setVisibility(View.VISIBLE);
                Map map = dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                String reciever = map.get("reciever").toString();
                String time = map.get("time").toString();
                if(reciever.equals(GlobalData.data.getCellNumber().replace("+", ""))){
                    adapter.addToStart(new Message(message, reciever, new Date(Long.parseLong(time)),
                            new Author("b")), true);
                }
                else{
                    adapter.addToStart(new Message(message, sender, new Date(Long.parseLong(time)),
                            new Author("a")), true);
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mixpanelAPI.flush();
    }
}
