package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

public class ChangePasswordModel{

    private String userType;
    private String cellNumber;
    private String oldPassword;
    private String newPassword;

    public ChangePasswordModel(String userType, String cellNumber, String oldPassword, String newPassword) {
        this.userType = userType;
        this.cellNumber = cellNumber;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }
}
