package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

import java.util.List;

public class SignUpModel {

    private String userType;
    private String firstName;
    private String sirName;
    private String cellNumber;
    private String password;
    private String gender;
    private String dob;
    private List<String> allergies = null;
    private List<String> conditions = null;
    private List<String> drugs = null;
    private String latitude, longitude;
    private String email;

    public SignUpModel(String userType, String firstName, String sirName, String cellNumber, String password,
                       String gender, String dob, List<String> allergies, List<String> conditions,
                       List<String> drugs, String latitude, String longitude, String email) {
        this.userType = userType;
        this.firstName = firstName;
        this.sirName = sirName;
        this.cellNumber = cellNumber;
        this.password = password;
        this.gender = gender;
        this.dob = dob;
        this.allergies = allergies;
        this.conditions = conditions;
        this.drugs = drugs;
        this.latitude = latitude;
        this.longitude = longitude;
        this.email = email;
    }
}
