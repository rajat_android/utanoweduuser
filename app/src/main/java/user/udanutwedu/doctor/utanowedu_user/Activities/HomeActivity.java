package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.media.AudioAttributesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import user.udanutwedu.doctor.utanowedu_user.BuildConfig;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.UI.Fragments.DoctorFragment;
import user.udanutwedu.doctor.utanowedu_user.UI.Fragments.EmergencyFragment;
import user.udanutwedu.doctor.utanowedu_user.Utilties.BottomNavigationHelper;
import user.udanutwedu.doctor.utanowedu_user.Utilties.Constants;
import user.udanutwedu.doctor.utanowedu_user.Utilties.GlobalData;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.PREF_NAME;

public class HomeActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private SharedPreferences sharedPreferences;
    private Firebase firebase;
    private MixpanelAPI mixpanelAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activty);
        mixpanelAPI = MixpanelAPI.getInstance(this, Constants.MIX_TOKEN);
        mixpanelAPI.track("UserApp - Landing Page");
        Firebase.setAndroidContext(this);
        setUpNotificationChannel();

        firebase = new Firebase("https://utanoweduchat-793fb.firebaseio.com/user");
        firebase.child(GlobalData.data.getCellNumber().replace("+", "")).child("userName").setValue(GlobalData.data.getFirstName()+" "+GlobalData.data.getSirName());
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        firebase.child(GlobalData.data.getCellNumber().replace("+", "")).child("token").setValue(task.getResult().getToken());
                    }
                });
        mTextMessage = (TextView) findViewById(R.id.message);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        DoctorFragment doctorFragment = new DoctorFragment();
        fragmentTransaction.replace(R.id.fragmentSurface, doctorFragment);
        fragmentTransaction.commit();
        getSupportActionBar().setTitle("Doctors");
    }

    private void setUpNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.chanel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("1015Utano", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_emergencies:
                    mixpanelAPI.track("UserApp - Click on Emergency Tab");
                    fragmentTransaction = fragmentManager.beginTransaction();
                    EmergencyFragment emergencyFragment = new EmergencyFragment();
                    fragmentTransaction.replace(R.id.fragmentSurface, emergencyFragment);
                    fragmentTransaction.commit();
                    getSupportActionBar().setTitle("Emergencies");
                    return true;

                case R.id.navigation_doctors:

                    fragmentTransaction = fragmentManager.beginTransaction();
                    DoctorFragment doctorFragment = new DoctorFragment();
                    fragmentTransaction.replace(R.id.fragmentSurface, doctorFragment);
                    fragmentTransaction.commit();
                    getSupportActionBar().setTitle("Doctors");
                    return true;

            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.homemenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.changePass:
                startActivity(new Intent(this, ChangePassword.class));
                break;
            case R.id.share:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    String shareMessage= "Let me recommend you this application\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID ;
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "Choose One"));
                } catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rate_app:
                try{
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                }
                catch (ActivityNotFoundException e){
                    Toast.makeText(this, "Google Play Store is not installed on this phone!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.logout:
                finishAffinity();
                startActivity(new Intent(this, LoginActivity.class));
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
                break;
            case R.id.chat:
                startActivity(new Intent(this, ChatListActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mixpanelAPI.flush();
    }
}
