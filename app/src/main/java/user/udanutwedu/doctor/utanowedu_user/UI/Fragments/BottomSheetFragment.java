package user.udanutwedu.doctor.utanowedu_user.UI.Fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ProviderDetailsResponse;
import user.udanutwedu.doctor.utanowedu_user.Interfaces.Api;
import user.udanutwedu.doctor.utanowedu_user.R;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private Button navigate, call;
    private ProviderDetailsResponse.Object object;
    private CircleImageView userimage;
    private TextView name;
    private TextView specialization;
    private TextView address;

    public BottomSheetFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        object = (ProviderDetailsResponse.Object) bundle.getSerializable("doctorData");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);
        navigate = view.findViewById(R.id.navigate);
        call = view.findViewById(R.id.call);
        userimage = view.findViewById( R.id.userimage );
        name = view.findViewById( R.id.name );
        specialization = view.findViewById( R.id.specialization );
        address = view.findViewById( R.id.address );
        name.setText(object.getFirstName()+" "+object.getSirName());
        specialization.setText(object.getProviderType()+"("+object.getCellNumber()+")");
        address.setText(object.getAddress());
        navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?daddr="+object.getLattitude()+","+object.getLongitude());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", object.getCellNumber(),
                        null));
                startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
            }
        });
        if(object.getProfilePic()!=null){
            Picasso.get().load(Api.BASE_URL+object.getProfilePic())
                    .placeholder(R.drawable.ic_user)
                    .into(userimage);
        }
        return view;
    }
}
