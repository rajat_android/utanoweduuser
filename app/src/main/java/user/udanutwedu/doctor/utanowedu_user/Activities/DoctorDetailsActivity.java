package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ProviderDetailsResponse;
import user.udanutwedu.doctor.utanowedu_user.Interfaces.Api;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.UI.Adapter.TimeAdapter;
import user.udanutwedu.doctor.utanowedu_user.Utilties.GlobalData;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.MIX_TOKEN;

public class DoctorDetailsActivity extends AppCompatActivity implements View.OnClickListener{

    Toolbar toolbar;
    private ImageView whatsapp;
    private ImageView linkedin;
    private ImageView facebook;
    private ImageView twitter, userimage, call;
    private ProviderDetailsResponse.Object object;
    private CollapsingToolbarLayout collapsingToolbar;
    private TextView name;
    private TextView specialization;
    private TextView professionalStatement, education;
    private TextView professionalMembership;
    private RecyclerView recyclerView;
    private TextView address;
    private MixpanelAPI mixpanelAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_details);
        toolbar = findViewById(R.id.toolbar);
        Firebase.setAndroidContext(this);
        mixpanelAPI = MixpanelAPI.getInstance(this, MIX_TOKEN);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        object = (ProviderDetailsResponse.Object) getIntent().getSerializableExtra("doctorData");
        whatsapp = findViewById( R.id.whatsapp );
        linkedin = findViewById( R.id.linkedin );
        facebook = findViewById( R.id.facebook );
        twitter = findViewById( R.id.twitter );
        userimage = findViewById(R.id.userimage );
        recyclerView =  findViewById(R.id.recyclerView);
        address = findViewById(R.id.address);
        call = findViewById(R.id.call);
        call.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        education = findViewById(R.id.education);
        collapsingToolbar = findViewById( R.id.collapsingToolbar );
        name = findViewById( R.id.name );
        specialization = findViewById( R.id.specialization );
        professionalStatement = findViewById( R.id.professionalStatement );
        professionalMembership = findViewById( R.id.professionalMembership );
        whatsapp.setOnClickListener(this);
        if(object.getLinkdinHandle()!=null){
            linkedin.setVisibility(View.VISIBLE);
           linkedin.setOnClickListener(this);
        }
        if(object.getFacebookHandle()!=null){
            facebook.setVisibility(View.VISIBLE);
            facebook.setOnClickListener(this);
        }
        if(object.getTwitterHandle()!=null){
            twitter.setVisibility(View.VISIBLE);
            twitter.setOnClickListener(this);
        }
        List<ProviderDetailsResponse.Object.Schedule> scheduleList = new ArrayList<>();
        for(ProviderDetailsResponse.Object.Schedule schedule : object.getSchedule()){
            if(schedule.getOpeningTime()!=null && schedule.getClosingTime()!=null){
                scheduleList.add(schedule);
            }
        }
        recyclerView.setAdapter(new TimeAdapter(scheduleList, this));
        if(object.getProfilePic()!=null){
            Picasso.get().load(Api.BASE_URL+object.getProfilePic())
                    .placeholder(R.drawable.ic_user)
                    .into(userimage);
        }
        if(object.getAddress()!=null){
            address.setText(object.getAddress());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        collapsingToolbar.setTitle(object.getFirstName()+" "+object.getSirName());
        name.setText(object.getFirstName()+" "+object.getSirName());
        specialization.setText(object.getSpecialization()+" ("+object.getCellNumber()+")");
        if(object.getProfessionalStatement()!=null)
            professionalStatement.setText(object.getProfessionalStatement());
        if(object.getProfessionalMemberships().size()!=0)
            professionalMembership.setText(object.getProfessionalMemberships().get(0));
        if(object.getQualifications()!=null)
            education.setText(object.getQualifications());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chatmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.whatsapp:
                mixpanelAPI.track("UserApp - Click on Whatsapp");
                try{
                    String url = "https://api.whatsapp.com/send?phone="+object.getCellNumber();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
                break;
            case R.id.linkedin:
                try{
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+object.getLinkdinHandle()));
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
                break;
            case R.id.facebook:
                try{
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+object.getFacebookHandle()));
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
                break;
            case R.id.twitter:
                try{
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+object.getTwitterHandle()));
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
                break;
            case R.id.call:
                try{
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", object.getCellNumber(),
                            null));
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.chat){

        Firebase firebase1 = new Firebase("https://utanoweduchat-793fb.firebaseio.com/user/"+ GlobalData.data.getCellNumber().replace("+", "")+"/ChattedWith");
        firebase1.child(object.getCellNumber().replace("+", ""));
        firebase1.child(object.getCellNumber().replace("+", "")).child("doctorName").setValue(object.getFirstName()+" "+object.getSirName());
        firebase1.child(object.getCellNumber().replace("+", "")).child("doctorNumber").setValue(object.getCellNumber().replace("+", ""));

        Firebase firebase2 = new Firebase("https://utanoweduchat-793fb.firebaseio.com/provider/"+object.getCellNumber().replace("+", "")+"/ChattedWith");
        firebase2.child(GlobalData.data.getCellNumber().replace("+", ""));
        firebase2.child(GlobalData.data.getCellNumber().replace("+", "")).child("userName").setValue(GlobalData.data.getFirstName()+" "+GlobalData.data.getSirName());
        firebase2.child(GlobalData.data.getCellNumber().replace("+", "")).child("userNumber").setValue(GlobalData.data.getCellNumber().replace("+", ""));

        startActivity(new Intent(this, ChatActivity.class).putExtra("doctorNumber", object.getCellNumber())
                .putExtra("doctorName", object.getFirstName()+" "+object.getSirName()));
        }
        else if(item.getItemId()==R.id.map){
            startActivity(new Intent(this, SinglDoctorScreen.class)
                    .putExtra("object", object));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mixpanelAPI.flush();
    }
}
