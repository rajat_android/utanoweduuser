package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

public class ForgotModel {

    private String userType;
    private String cellNumber;
    private String email;

    public ForgotModel(String userType, String cellNumber, String email) {
        this.userType = userType;
        this.cellNumber = cellNumber;
        this.email = email;
    }
}
