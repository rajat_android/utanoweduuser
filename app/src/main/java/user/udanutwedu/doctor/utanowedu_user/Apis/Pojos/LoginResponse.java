package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access-token")
    @Expose
    private String accessToken;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data{
        @SerializedName("allergies")
        @Expose
        private List<Item> allergies = null;
        @SerializedName("conditions")
        @Expose
        private List<Item> conditions = null;
        @SerializedName("drugs")
        @Expose
        private List<Item> drugs = null;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;
        @SerializedName("sirName")
        @Expose
        private String sirName;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("userType")
        @Expose
        private String userType;
        @SerializedName("cellNumber")
        @Expose
        private String cellNumber;
        @SerializedName("_id")
        @Expose
        private String id;

        public List<Item> getAllergies() {
            return allergies;
        }

        public void setAllergies(List<Item> allergies) {
            this.allergies = allergies;
        }

        public List<Item> getConditions() {
            return conditions;
        }

        public void setConditions(List<Item> conditions) {
            this.conditions = conditions;
        }

        public List<Item> getDrugs() {
            return drugs;
        }

        public void setDrugs(List<Item> drugs) {
            this.drugs = drugs;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getSirName() {
            return sirName;
        }

        public void setSirName(String sirName) {
            this.sirName = sirName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getCellNumber() {
            return cellNumber;
        }

        public void setCellNumber(String cellNumber) {
            this.cellNumber = cellNumber;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public class Item{
            @SerializedName("_id")
            @Expose
            String id;
            @SerializedName("name")
            @Expose
            String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
