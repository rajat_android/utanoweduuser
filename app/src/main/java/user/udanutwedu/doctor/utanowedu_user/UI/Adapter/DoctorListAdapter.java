package user.udanutwedu.doctor.utanowedu_user.UI.Adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import user.udanutwedu.doctor.utanowedu_user.Activities.DoctorDetailsActivity;
import user.udanutwedu.doctor.utanowedu_user.Activities.SinglDoctorScreen;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ProviderDetailsResponse;
import user.udanutwedu.doctor.utanowedu_user.Interfaces.Api;
import user.udanutwedu.doctor.utanowedu_user.R;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.MIX_TOKEN;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.ViewHolder>{

    private Context context;
    private List<ProviderDetailsResponse.Object> nameList, nameListHelper;
    private MixpanelAPI mixpanelAPI;

    public DoctorListAdapter(Context context, List<ProviderDetailsResponse.Object> nameList) {
        this.context = context;
        this.nameList = nameList;
        this.nameListHelper = new ArrayList<>(nameList);
        mixpanelAPI = MixpanelAPI.getInstance(context, MIX_TOKEN);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.name.setText(nameListHelper.get(position).getFirstName()+" "+nameListHelper.get(position).getSirName());
        holder.specialization.setText(nameListHelper.get(position).getSpecialization()+" ("+nameListHelper.get(position).getCellNumber()+")");
        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DoctorDetailsActivity.class).putExtra("doctorData", nameListHelper.get(position)));
            }
        });
        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mixpanelAPI.track("UserApp - Click on Whatsapp");
                try{
                    String url = "https://api.whatsapp.com/send?phone="+nameListHelper.get(position).getCellNumber();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    context.startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
            }
        });
        if(nameListHelper.get(position).getFacebookHandle()!=null){
            holder.facebook.setVisibility(View.VISIBLE);
            holder.facebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+nameListHelper.get(position).getFacebookHandle()));
                        context.startActivity(intent);
                    }
                    catch (ActivityNotFoundException e){
                        e.printStackTrace();
                    }
                }
            });
        }
        if(nameListHelper.get(position).getLinkdinHandle()!=null){
            holder.linkedIn.setVisibility(View.VISIBLE);
            holder.linkedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    try{
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+nameListHelper.get(position).getLinkdinHandle()));
                    context.startActivity(intent);
                    }
                    catch (ActivityNotFoundException e){
                        e.printStackTrace();
                    }
                }
            });
        }
        if(nameListHelper.get(position).getTwitterHandle()!=null){
            holder.twitter.setVisibility(View.VISIBLE);
            holder.twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    try{
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+nameListHelper.get(position).getTwitterHandle()));
                    context.startActivity(intent);
                    }
                    catch (ActivityNotFoundException e){
                        e.printStackTrace();
                    }
            }
            });
        }
        if(nameListHelper.get(position).getProfilePic()!=null){
            Picasso.get().load(Api.BASE_URL+nameListHelper.get(position).getProfilePic())
                    .placeholder(R.drawable.ic_user)
                    .into(holder.userimage);
        }
        if(nameListHelper.get(position).getAddress()!=null)
        holder.address.setText(nameListHelper.get(position).getAddress());
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", nameListHelper.get(position).getCellNumber(),
                            null));
                    context.startActivity(intent);
                }
                catch (ActivityNotFoundException e){
                    e.printStackTrace();
                }
            }
        });
        holder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, SinglDoctorScreen.class)
                .putExtra("object", nameListHelper.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return nameListHelper.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout parent_layout;
        private TextView name, specialization, address;
        private ImageView whatsapp, facebook, twitter, linkedIn, userimage, call, location;

        public ViewHolder(View itemView) {
            super(itemView);
            parent_layout    = itemView.findViewById(R.id.parent_layout);
            name = itemView.findViewById(R.id.name);
            whatsapp = itemView.findViewById(R.id.whatsapp);
            facebook = itemView.findViewById(R.id.facebook);
            twitter = itemView.findViewById(R.id.twitter);
            linkedIn = itemView.findViewById(R.id.linkedin);
            specialization = itemView.findViewById(R.id.specialization);
            address = itemView.findViewById(R.id.address);
            userimage = itemView.findViewById(R.id.userimage);
            call = itemView.findViewById(R.id.call);
            location = itemView.findViewById(R.id.location);
        }
    }

    public void filter(String srNum) {
        nameListHelper.clear();
        for (ProviderDetailsResponse.Object obj : nameList) {
            if ((obj.getFirstName()+" "+obj.getSirName()).toLowerCase().contains(srNum.toLowerCase())) {
                nameListHelper.add(obj);
            }
        }
        notifyDataSetChanged();
    }


    public void destroy(){
        mixpanelAPI.flush();
    }
}
