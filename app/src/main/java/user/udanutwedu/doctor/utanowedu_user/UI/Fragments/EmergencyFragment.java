package user.udanutwedu.doctor.utanowedu_user.UI.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import user.udanutwedu.doctor.utanowedu_user.Activities.DoctorListActivity;
import user.udanutwedu.doctor.utanowedu_user.Activities.Emergencies_Map_Activity;
import user.udanutwedu.doctor.utanowedu_user.Activities.HomeActivity;
import user.udanutwedu.doctor.utanowedu_user.R;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.SPECILIZATION;
import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.TYPE;

public class EmergencyFragment extends Fragment implements View.OnClickListener{

    private LinearLayout ambulances;
    private LinearLayout accidentErs;
    private LinearLayout suicide;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_emergency, container, false);
        suicide = view.findViewById( R.id.suicide );
        accidentErs = view.findViewById( R.id.accident_ers );
        ambulances = view.findViewById( R.id.ambulances );
        accidentErs.setOnClickListener(this);
        suicide.setOnClickListener(this);
        ambulances.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ambulances:
                startActivity(new Intent(getContext(), DoctorListActivity.class)
                        .putExtra(SPECILIZATION, getString(R.string.pharmacy))
                        .putExtra(TYPE, "Emergency Services"));
                break;
            case R.id.accident_ers:
                startActivity(new Intent(getContext(), DoctorListActivity.class)
                        .putExtra(SPECILIZATION, getString(R.string.accident_rooms))
                        .putExtra(TYPE, "Emergency Services"));
                break;
            case R.id.suicide:
                startActivity(new Intent(getContext(), DoctorListActivity.class)
                        .putExtra(SPECILIZATION, getString(R.string.suicide))
                        .putExtra(TYPE, "Emergency Services"));
                break;
        }
    }
}
