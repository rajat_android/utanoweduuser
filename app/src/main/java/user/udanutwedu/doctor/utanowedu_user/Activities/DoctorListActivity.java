package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.udanutwedu.doctor.utanowedu_user.Apis.CRetrofit;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ProviderDetailsResponse;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.UI.Adapter.DoctorListAdapter;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.SPECILIZATION;
import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.TYPE;

public class DoctorListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private EditText searchview;
    private DoctorListAdapter doctorListAdapter;
    private Call<ProviderDetailsResponse> call;
    private List<ProviderDetailsResponse.Object> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);
        searchview = findViewById(R.id.et_search);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        list = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProviderList(getIntent().getStringExtra(SPECILIZATION), getIntent().getStringExtra(TYPE));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra(SPECILIZATION));
        getProviderList(getIntent().getStringExtra(SPECILIZATION), getIntent().getStringExtra(TYPE));
        searchview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(doctorListAdapter!=null)
                doctorListAdapter.filter(s.toString());
            }
        });
    }

    private void getProviderList(String specialization, String providerType) {
        call = CRetrofit.getInstance().getProvidersList(providerType, specialization);
        call.enqueue(new Callback<ProviderDetailsResponse>() {
            @Override
            public void onResponse(Call<ProviderDetailsResponse> call, Response<ProviderDetailsResponse> response) {
                if(response.code()==200){
                    recyclerView.setAdapter(doctorListAdapter = new DoctorListAdapter(DoctorListActivity.this,
                            list = response.body().getObject()));
                }
                else{
                    Toast.makeText(DoctorListActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ProviderDetailsResponse> call, Throwable t) {
                Log.e("throw", t.toString());
                Toast.makeText(DoctorListActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.maps_doctors:
                if(list.size()!=0){
                    startActivity(new Intent(this, Emergencies_Map_Activity.class)
                            .putExtra("doctorList", (Serializable) list));
                }
                else{
                    Toast.makeText(this, "No Doctors to Show Currently!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(call!=null){
            call.cancel();
        }
        if(doctorListAdapter!=null){
            doctorListAdapter.destroy();
        }
    }
}
