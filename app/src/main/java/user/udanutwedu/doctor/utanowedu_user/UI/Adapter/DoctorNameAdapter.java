package user.udanutwedu.doctor.utanowedu_user.UI.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import user.udanutwedu.doctor.utanowedu_user.Activities.DoctorListActivity;
import user.udanutwedu.doctor.utanowedu_user.R;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.SPECILIZATION;
import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.TYPE;

public class DoctorNameAdapter extends RecyclerView.Adapter<DoctorNameAdapter.ViewHolder>{

    private Context context;
    private List<String> doctorNames;

    public DoctorNameAdapter(Context context, List<String> doctorNames) {
        this.context = context;
        this.doctorNames = doctorNames;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_name_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.doctorType.setText(doctorNames.get(position));
        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DoctorListActivity.class)
                        .putExtra(SPECILIZATION, doctorNames.get(position))
                .putExtra(TYPE, "Doctor"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return doctorNames.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView doctorType;
        LinearLayout parent_layout;

        public ViewHolder(View itemView) {
            super(itemView);
            doctorType = itemView.findViewById(R.id.doctor_type);
            parent_layout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
