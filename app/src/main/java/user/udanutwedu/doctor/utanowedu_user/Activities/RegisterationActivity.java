package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.udanutwedu.doctor.utanowedu_user.Apis.CRetrofit;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.AllergiesResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.SignUpModel;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.SignUpResponse;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.Utilties.MultiSelectSpinner;
import user.udanutwedu.doctor.utanowedu_user.Utilties.MyFunctions;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.MIX_TOKEN;

public class RegisterationActivity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private EditText firstName;
    private EditText lastName;
    private EditText cellNumber;
    private EditText password;
    private RadioGroup genderGroup;
    private EditText dob, email;
    private MultiSelectSpinner allergies;
    private MultiSelectSpinner conditions;
    private MultiSelectSpinner drugs;
    private CheckBox tnC;
    private String gender = "";
    private Button verifyButton, submit;
    private ProgressDialog progressDialog;
    private EditText address;
    private LocationRequest locationRequest;
    private GoogleApiClient mGoogleApiClient;
    private final static int MY_PERMISSIONS_REQUEST_LOCATION = 2000;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 1000;
    private TextView searchAddress;
    private Double lat, lng;
    private LatLng currentLocation, sendLocation;
    private boolean getCurrentLocation = true;
    private Call<SignUpResponse> signUpResponseCall;
    private Call<AllergiesResponse> call;
    private MixpanelAPI mixpanelAPI;
//    private Boolean isNumberVerified = false;
//    private Dialog dialog;
//    private EditText e1, e2, e3, e4;
    final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private String[] countryCodes = {"+263", "+91"};
    private Spinner codeSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);
        findViews();
        getSupportActionBar().setTitle("Utano Wedu");
        mixpanelAPI = MixpanelAPI.getInstance(this, MIX_TOKEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sendLocation = new LatLng(0.0, 0.0);
        codeSpinner = findViewById(R.id.codeSpinner);
        codeSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countryCodes));
        /*dialog = new Dialog(this);
        dialog.setContentView(R.layout.my_custom_dialog);
        dialog.setCancelable(false);
        e1 = dialog.findViewById(R.id.e1);
        e2 = dialog.findViewById(R.id.e2);
        e3 = dialog.findViewById(R.id.e3);
        e4 = dialog.findViewById(R.id.e4);
        submit = dialog.findViewById(R.id.submitOTP);
        addTextWatcher(e1, e2);
        addTextWatcher(e2, e3);
        addTextWatcher(e3, e4);
        addTextWatcher(e4, null);*/
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait!");
        email = findViewById(R.id.email);

//        submit.setOnClickListener(this);

        genderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId==R.id.male){
                    gender = "male";
                }
                else if(checkedId==R.id.female){
                    gender = "female";
                }
                else{
                    gender = "other";
                }
            }
        });

        dob.setOnClickListener(this);
        getLists();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds

        mGoogleApiClient.connect();

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().equalsIgnoreCase("")){
                    sendLocation = currentLocation;
                    getCurrentLocation = true;
                }
            }
        });

    }

    private void getLists() {
        progressDialog.show();
        call = CRetrofit.getInstance().getAllergiesList();
        call.enqueue(new Callback<AllergiesResponse>() {
            @Override
            public void onResponse(Call<AllergiesResponse> call, Response<AllergiesResponse> response) {
                if(response.isSuccessful()){
                   conditions.setItems(response.body().getObject().getConditions());
                   allergies.setItems(response.body().getObject().getAllergies());
                   drugs.setItems(response.body().getObject().getDrugs());
                }
                else{
                    Toast.makeText(RegisterationActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<AllergiesResponse> call, Throwable t) {
                Log.e("throw", t.toString());
                Toast.makeText(RegisterationActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void findViews() {
        firstName = findViewById( R.id.firstName );
        lastName = findViewById( R.id.lastName );
        cellNumber = findViewById( R.id.cell_number );
        password = findViewById( R.id.password );
        genderGroup = findViewById( R.id.genderGroup );
        dob = findViewById( R.id.dob );
        allergies = findViewById( R.id.allergies );
        conditions = findViewById( R.id.conditions );
        drugs = findViewById( R.id.drugs );
        tnC = findViewById( R.id.tnC );
        address = findViewById(R.id.address);
        searchAddress = findViewById(R.id.searchAddress);
        searchAddress.setOnClickListener(this);
        address.setOnClickListener(this);
//        verifyButton = findViewById(R.id.verifyButton);
//        verifyButton.setOnClickListener(this);
        findViewById( R.id.registerButton ).setOnClickListener( this );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.registerButton:
                    if(validate()){
                        progressDialog.show();
                        signUpResponseCall = CRetrofit.getInstance().registerUser(new SignUpModel("user",
                                firstName.getText().toString().trim(), lastName.getText().toString(), codeSpinner.getSelectedItem().toString()+cellNumber.getText().toString(), password.getText().toString(),
                                gender, dob.getText().toString(), allergies.getSelectedStrings(), conditions.getSelectedStrings(), drugs.getSelectedStrings(),
                                String.valueOf(sendLocation.latitude), String.valueOf(sendLocation.longitude), email.getText().toString()));
                        signUpResponseCall.enqueue(new Callback<SignUpResponse>() {
                            @Override
                            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                                if(response.code() == 200){
                                    mixpanelAPI.track("User App - Registration");
                                    new AlertDialog.Builder(RegisterationActivity.this)
                                            .setCancelable(false)
                                            .setMessage(response.body().getMessage())
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                                }
                                            }).show();
                                }
                                else{
                                    try {
                                        MyFunctions.dialogdontfinish(new JSONObject(response.errorBody().string()).getString("message"), RegisterationActivity.this);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                Log.e("throw", t.toString());
                                Toast.makeText(RegisterationActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                break;
            case R.id.dob:
                DatePickerDialog datePickerDialog = MyFunctions.setUpDatePicker(dob, this);
                datePickerDialog.show();
                break;
//            case R.id.verifyButton:
//                dialog.show();
//                break;
            case R.id.submitOTP:
                /*verifyButton.setText("Verified");
                verifyButton.setEnabled(false);
                dialog.dismiss();*/
                break;
            case R.id.searchAddress:
                startActivityForResult(new Intent(this, GetLocationActivity.class).putExtra("lat",
                        currentLocation.latitude).putExtra("long", currentLocation.longitude), 5);
                break;
        }
    }

    private boolean validate() {
        boolean isDataOk = true;
        if(TextUtils.isEmpty(cellNumber.getText().toString().trim()) || cellNumber.getText().toString().trim().length()!=9){
            cellNumber.setError("Mendatory Field!");
            isDataOk = false;
        }
        if(TextUtils.isEmpty(lastName.getText().toString().trim())){
            lastName.setError("Mendatory Field!");
            isDataOk = false;
        }
        if(TextUtils.isEmpty(password.getText().toString().trim())){
            password.setError("Mendatory Field!");
            isDataOk = false;
        }
        if(TextUtils.isEmpty(gender)){
            Toast.makeText(this, "Please select gender!", Toast.LENGTH_SHORT).show();
            isDataOk = false;
        }
        if(TextUtils.isEmpty(dob.getText().toString().trim())){
            dob.setError("Mendatory Field!");
            isDataOk = false;
        }
        if(TextUtils.isEmpty(address.getText().toString().trim())){
            address.setError("Mendatory Field!");
            isDataOk = false;
        }
        if (TextUtils.isEmpty(email.getText().toString().trim()) || !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()){
            email.setError(getString(R.string.invalid_email));
            isDataOk = false;
        }
        return isDataOk;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            sendLocation = currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if(connectionResult.hasResolution()){
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        }
        else{
            Log.e("throw", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(getCurrentLocation){
            if(sendLocation !=null){
            sendLocation = currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5 && resultCode == RESULT_OK){
            Integer type = data.getIntExtra("type", 0);
            String add = data.getStringExtra("address");
            Double latt = data.getDoubleExtra("lat", 0.0);
            Double longt = data.getDoubleExtra("long", 0.0);
            if(type==1){
                address.setText(add);
                sendLocation = new LatLng(latt, longt);
                getCurrentLocation = false;
            }
            else{
                address.setText("");
                sendLocation = currentLocation;
                Toast.makeText(this, "Please enter address manually!", Toast.LENGTH_SHORT).show();
                getCurrentLocation = true;
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    /*private void addTextWatcher(final EditText currentEditText, final EditText nextEditText) {
        currentEditText.addTextChangedListener(new android.text.TextWatcher() {

            @android.support.annotation.RequiresApi(api = android.os.Build.VERSION_CODES.KITKAT)
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (currentEditText.getText().toString().length() == 1) {     //size as per your requirement
                    if (nextEditText != null)
                        nextEditText.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(android.text.Editable s) {

            }

        });
    }*/

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
        if(call!=null){
            call.cancel();
        }
        if(signUpResponseCall!=null){
            signUpResponseCall.cancel();
        }
        mixpanelAPI.flush();
    }

}
