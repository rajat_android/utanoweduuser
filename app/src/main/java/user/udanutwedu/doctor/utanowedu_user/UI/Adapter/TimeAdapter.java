package user.udanutwedu.doctor.utanowedu_user.UI.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.ProviderDetailsResponse;
import user.udanutwedu.doctor.utanowedu_user.R;

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.ViewHolder>{

    List<ProviderDetailsResponse.Object.Schedule> list;
    Context context;

    public TimeAdapter(List<ProviderDetailsResponse.Object.Schedule> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.time_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.day.setText(list.get(position).getDay().substring(0,1).toUpperCase()+list.get(position).getDay().substring(1));
        holder.timing.setText((list.get(position).getOpeningTime()!=null?list.get(position).getOpeningTime():"")+" to "+
                (list.get(position).getClosingTime()!=null?list.get(position).getClosingTime():""));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView day;
        private TextView timing;

        public ViewHolder(View itemView) {
            super(itemView);
            day = itemView.findViewById( R.id.day );
            timing = itemView.findViewById( R.id.timing );
        }
    }
}
