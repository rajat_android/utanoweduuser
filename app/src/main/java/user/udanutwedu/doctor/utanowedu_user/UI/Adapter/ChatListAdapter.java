package user.udanutwedu.doctor.utanowedu_user.UI.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import user.udanutwedu.doctor.utanowedu_user.Activities.ChatActivity;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.Doctor;
import user.udanutwedu.doctor.utanowedu_user.R;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder>{

    private Context context;
    private List<Doctor> doctorList;

    public ChatListAdapter(Context context, List<Doctor> doctorList) {
        this.context = context;
        this.doctorList = doctorList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ChatActivity.class).putExtra("doctorNumber", doctorList.get(position).getDoctorNumber())
                .putExtra("doctorName", doctorList.get(position).getDoctorName()));
            }
        });
        holder.name.setText(doctorList.get(position).getDoctorName());
        holder.number.setText(doctorList.get(position).getDoctorNumber());
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return doctorList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout parent_layout;
        private TextView name, number;
        public ViewHolder(View itemView) {
            super(itemView);
            parent_layout = itemView.findViewById(R.id.parent_layout);
            name =  itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.number);
        }
    }
}
