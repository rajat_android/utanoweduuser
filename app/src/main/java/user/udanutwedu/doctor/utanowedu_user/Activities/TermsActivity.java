package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import user.udanutwedu.doctor.utanowedu_user.R;

public class TermsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        String name;
        getSupportActionBar().setTitle(name = getIntent().getStringExtra("name"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        WebView webView = findViewById(R.id.webview);
        if(name.equalsIgnoreCase(getString(R.string.policy))){
            webView.loadUrl("file:///android_asset/privacy.html");
        }
        else if(name.equalsIgnoreCase(getString(R.string.terms))){
            webView.loadUrl("file:///android_asset/terms.html");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
