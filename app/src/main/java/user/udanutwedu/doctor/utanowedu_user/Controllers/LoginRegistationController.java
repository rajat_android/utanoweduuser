/*
package user.udanutwedu.doctor.utanowedu_user.Controllers;

import android.content.Context;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import user.udanutwedu.doctor.utanowedu_user.Apis.CRetrofit;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginBody;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginResponse;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.RegistrationBody;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.RegistrationResponse;
import user.udanutwedu.doctor.utanowedu_user.Interfaces.ResponseInterface;
import user.udanutwedu.doctor.utanowedu_user.Utilties.Constants;

public class LoginRegistationController {

    private ResponseInterface loginInterface;

    public LoginRegistationController(Context context) {
        this.loginInterface = (ResponseInterface) context;
    }

    public void loginApiCall(String cellNumber, String password){

        Call<LoginResponse> call = CRetrofit.getInstance().login(new LoginBody(cellNumber, password));
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){
                   if(response.body().getStatus().equalsIgnoreCase("0")){
                       loginInterface.success();
                       Constants.token = response.body().getToken();
                       Constants.userDetails = response.body().getUserDetails();
                   }
                   else{
                       loginInterface.otherMessage(response.body().getMessage());
                   }
                }
                else{
                    loginInterface.newtworkIssue("Internal Server Error");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginInterface.newtworkIssue(t.toString());
            }
        });
    }

    public void registerApiCall(RegistrationBody registrationBody){

       Call<RegistrationResponse> call = CRetrofit.getInstance().register(registrationBody);
       call.enqueue(new Callback<RegistrationResponse>() {
           @Override
           public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
               if(response.isSuccessful()){
                   if(response.body().getStatus().equalsIgnoreCase("0")){
                       loginInterface.success();
                   }
                   else{
                       loginInterface.otherMessage(response.body().getMessage());
                   }
               }
               else{
                   loginInterface.newtworkIssue("Internal Server Error");
               }
           }

           @Override
           public void onFailure(Call<RegistrationResponse> call, Throwable t) {
               loginInterface.newtworkIssue(t.toString());
           }
       });
    }
}
*/
