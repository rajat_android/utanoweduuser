package user.udanutwedu.doctor.utanowedu_user.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.Doctor;
import user.udanutwedu.doctor.utanowedu_user.Apis.Pojos.LoginResponse;
import user.udanutwedu.doctor.utanowedu_user.R;
import user.udanutwedu.doctor.utanowedu_user.UI.Adapter.ChatListAdapter;
import user.udanutwedu.doctor.utanowedu_user.Utilties.GlobalData;

import static user.udanutwedu.doctor.utanowedu_user.Utilties.Constants.PREF_NAME;

public class ChatListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Doctor> doctorList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        getSupportActionBar().setTitle(R.string.chat);
        Gson gson = new Gson();
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String stringData = sharedPreferences.getString("data", "");
        GlobalData.data = gson.fromJson(stringData, LoginResponse.Data.class);
        Firebase.setAndroidContext(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        doctorList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getChatList();
            }
        });
        getChatList();
    }

    private void getChatList() {
        Firebase firebase = new Firebase("https://utanoweduchat-793fb.firebaseio.com/user/"+ GlobalData.data.getCellNumber().replace("+", "")+"/ChattedWith");
        firebase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getNameList((Map<String, Object>) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(ChatListActivity.this, R.string.connect_issue, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void getNameList(Map<String, Object> value) {
        doctorList.clear();
        if(value!=null){
            for(Map.Entry<String, Object> entry : value.entrySet()){
                Map singleUser = (Map) entry.getValue();
                if(singleUser.containsKey("doctorName") && singleUser.containsKey("doctorNumber")){
                    doctorList.add(new Doctor(singleUser.get("doctorName").toString(), singleUser.get("doctorNumber").toString()));
                }
            }
            recyclerView.setAdapter(new ChatListAdapter(this, doctorList));
        }
        else{
            Toast.makeText(this, "No Chats to Show!", Toast.LENGTH_SHORT).show();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
