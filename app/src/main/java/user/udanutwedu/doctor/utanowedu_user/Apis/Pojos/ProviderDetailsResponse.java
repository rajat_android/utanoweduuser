package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProviderDetailsResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("object")
    @Expose
    private List<Object> object = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("extendedMessage")
    @Expose
    private String extendedMessage;
    @SerializedName("timeStamp")
    @Expose
    private Long timeStamp;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<Object> getObject() {
        return object;
    }

    public void setObject(List<Object> object) {
        this.object = object;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExtendedMessage() {
        return extendedMessage;
    }

    public void setExtendedMessage(String extendedMessage) {
        this.extendedMessage = extendedMessage;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public class Object implements Serializable{
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("cellNumber")
        @Expose
        private String cellNumber;
        @SerializedName("userType")
        @Expose
        private String userType;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("createdOn")
        @Expose
        private String createdOn;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("professionalStatement")
        @Expose
        private String professionalStatement;
        @SerializedName("providerType")
        @Expose
        private String providerType;
        @SerializedName("sirName")
        @Expose
        private String sirName;
        @SerializedName("specialization")
        @Expose
        private String specialization;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;
        @SerializedName("lattitude")
        @Expose
        private String lattitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("schedule")
        @Expose
        private List<Schedule> schedule = null;
        @SerializedName("twitterHandle")
        @Expose
        private String twitterHandle;
        @SerializedName("facebookHandle")
        @Expose
        private String facebookHandle;
        @SerializedName("linkdinHandle")
        @Expose
        private String linkdinHandle;
        @SerializedName("profilePic")
        @Expose
        private String profilePic;

        public String getProfilePic() {
            return profilePic;
        }

        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        public String getTwitterHandle() {
            return twitterHandle;
        }

        public void setTwitterHandle(String twitterHandle) {
            this.twitterHandle = twitterHandle;
        }

        public String getFacebookHandle() {
            return facebookHandle;
        }

        public void setFacebookHandle(String facebookHandle) {
            this.facebookHandle = facebookHandle;
        }

        public String getLinkdinHandle() {
            return linkdinHandle;
        }

        public void setLinkdinHandle(String linkdinHandle) {
            this.linkdinHandle = linkdinHandle;
        }

        @SerializedName("professionalMemberships")
        @Expose
        private List<String> professionalMemberships = null;
        @SerializedName("qualifications")
        @Expose
        private String qualifications = null;

        public String getLattitude() {
            return lattitude;
        }

        public void setLattitude(String lattitude) {
            this.lattitude = lattitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCellNumber() {
            return cellNumber;
        }

        public void setCellNumber(String cellNumber) {
            this.cellNumber = cellNumber;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getProfessionalStatement() {
            return professionalStatement;
        }

        public void setProfessionalStatement(String professionalStatement) {
            this.professionalStatement = professionalStatement;
        }

        public String getProviderType() {
            return providerType;
        }

        public void setProviderType(String providerType) {
            this.providerType = providerType;
        }

        public String getSirName() {
            return sirName;
        }

        public void setSirName(String sirName) {
            this.sirName = sirName;
        }

        public String getSpecialization() {
            return specialization;
        }

        public void setSpecialization(String specialization) {
            this.specialization = specialization;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<Schedule> getSchedule() {
            return schedule;
        }

        public void setSchedule(List<Schedule> schedule) {
            this.schedule = schedule;
        }

        public List<String> getProfessionalMemberships() {
            return professionalMemberships;
        }

        public void setProfessionalMemberships(List<String> professionalMemberships) {
            this.professionalMemberships = professionalMemberships;
        }


        public String getQualifications() {
            return qualifications;
        }

        public void setQualifications(String qualifications) {
            this.qualifications = qualifications;
        }

        public class Schedule implements Serializable{

            @SerializedName("_id")
            @Expose
            private String id;
            @SerializedName("openingTime")
            @Expose
            private String openingTime;
            @SerializedName("day")
            @Expose
            private String day;
            @SerializedName("closingTime")
            @Expose
            private String closingTime;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getOpeningTime() {
                return openingTime;
            }

            public void setOpeningTime(String openingTime) {
                this.openingTime = openingTime;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public String getClosingTime() {
                return closingTime;
            }

            public void setClosingTime(String closingTime) {
                this.closingTime = closingTime;
            }
        }
    }
}
