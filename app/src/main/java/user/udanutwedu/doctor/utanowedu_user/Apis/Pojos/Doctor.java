package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

public class Doctor {

    String doctorName;
    String doctorNumber;

    public Doctor(String doctorName, String doctorNumber) {
        this.doctorName = doctorName;
        this.doctorNumber = doctorNumber;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public String getDoctorNumber() {
        return doctorNumber;
    }
}
