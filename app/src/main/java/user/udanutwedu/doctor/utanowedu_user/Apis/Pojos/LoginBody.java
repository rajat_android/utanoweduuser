package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

public class LoginBody {

    private String cellNumber;
    private String password;
    private String userType;

    public LoginBody(String cellNumber, String password, String userType) {
        this.cellNumber = cellNumber;
        this.password = password;
        this.userType = userType;
    }
}
