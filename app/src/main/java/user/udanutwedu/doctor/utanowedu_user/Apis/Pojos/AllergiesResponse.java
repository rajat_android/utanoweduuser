package user.udanutwedu.doctor.utanowedu_user.Apis.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllergiesResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("object")
    @Expose
    private Object object;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("extendedMessage")
    @Expose
    private String extendedMessage;
    @SerializedName("timeStamp")
    @Expose
    private Long timeStamp;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExtendedMessage() {
        return extendedMessage;
    }

    public void setExtendedMessage(String extendedMessage) {
        this.extendedMessage = extendedMessage;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public class Object{
        @SerializedName("allergies")
        @Expose
        private List<Item> allergies = null;
        @SerializedName("conditions")
        @Expose
        private List<Item> conditions = null;
        @SerializedName("drugs")
        @Expose
        private List<Item> drugs = null;

        public List<Item> getAllergies() {
            return allergies;
        }

        public void setAllergies(List<Item> allergies) {
            this.allergies = allergies;
        }

        public List<Item> getConditions() {
            return conditions;
        }

        public void setConditions(List<Item> conditions) {
            this.conditions = conditions;
        }

        public List<Item> getDrugs() {
            return drugs;
        }

        public void setDrugs(List<Item> drugs) {
            this.drugs = drugs;
        }

        public class Item{
            @SerializedName("_id")
            @Expose
            String id;
            @SerializedName("name")
            @Expose
            String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

}
